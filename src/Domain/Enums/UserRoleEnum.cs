﻿using Ardalis.SmartEnum;

namespace AmphibiansRecordsBackend.Domain.Enums;

public sealed class UserRoleEnum : SmartEnum<UserRoleEnum>
{
    public static readonly UserRoleEnum User = new("User", 0);
    public static readonly UserRoleEnum Moder = new("Moder", 1);
    public static readonly UserRoleEnum Admin = new("Admin", 2);

    private UserRoleEnum(string name, int value) : base(name, value)
    {
    }
}