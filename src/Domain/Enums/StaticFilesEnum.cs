﻿using Ardalis.SmartEnum;

namespace AmphibiansRecordsBackend.Domain.Enums;

public class StaticFilesEnum : SmartEnum<StaticFilesEnum>
{
    public static readonly StaticFilesEnum HtmlConfirmMail = new("html_confirm_mail", Path.Combine("html", "email_confirm_mail.html"), 0);
    
    private StaticFilesEnum(string name, string fileName, int value) : base(name, value)
    {
        FileName = fileName;
    }

    public string FileName { get; }
}