﻿using FluentValidation;

namespace AmphibiansRecordsBackend.Application.Authorization.SharedModels;

public sealed class AuthRequestModel
{
    public string Email { get; set; }

    public string Password { get; set; }
}

public sealed class AuthRequestModelValidator : AbstractValidator<AuthRequestModel>
{
    public AuthRequestModelValidator()
    {
        RuleFor(x => x.Email).NotNull().NotEmpty().EmailAddress();
        RuleFor(x => x.Password).NotNull().NotEmpty().MinimumLength(6).MaximumLength(64);
    }
}