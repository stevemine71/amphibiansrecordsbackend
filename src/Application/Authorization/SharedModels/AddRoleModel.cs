﻿namespace AmphibiansRecordsBackend.Application.Authorization.SharedModels;

public sealed class AddRoleModel
{
    public string UserId { get; set; }

    public string Role { get; set; }
}