﻿using FluentValidation;

namespace AmphibiansRecordsBackend.Application.Authorization.Commands.ResetPassword.Models;

public sealed class ResetPasswordModel
{
    public string Email { get; set; }

    public string Password { get; set; }

    public string ConfirmPassword { get; set; }

    public string Code { get; set; }
}

public sealed class ResetPasswordModelValidator : AbstractValidator<ResetPasswordModel>
{
    public ResetPasswordModelValidator()
    {
        RuleFor(x => x.Email).NotNull().NotEmpty().EmailAddress();
        RuleFor(x => x.ConfirmPassword).NotNull().NotEmpty().MinimumLength(6).MaximumLength(128);
        RuleFor(x => x.Password).NotNull().NotEmpty().MinimumLength(6).MaximumLength(128).Equal(x => x.ConfirmPassword);
        RuleFor(x => x.Code).NotNull().NotEmpty();
    }
}