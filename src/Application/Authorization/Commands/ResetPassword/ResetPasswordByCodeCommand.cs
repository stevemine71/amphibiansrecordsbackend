﻿using AmphibiansRecordsBackend.Application.Authorization.Commands.ResetPassword.Models;
using AmphibiansRecordsBackend.Application.Common.Extensions;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using FluentValidation;
using MediatR;

namespace AmphibiansRecordsBackend.Application.Authorization.Commands.ResetPassword;

public record ResetPasswordByCodeCommand(ResetPasswordModel ResetPasswordModel) : IRequest<bool>;

public sealed class ResetPasswordByCodeCommandHandler : IRequestHandler<ResetPasswordByCodeCommand, bool>
{
    private readonly IResetPasswordService _resetPasswordService;

    public ResetPasswordByCodeCommandHandler(IResetPasswordService resetPasswordService)
    {
        _resetPasswordService = resetPasswordService;
    }

    public async Task<bool> Handle(ResetPasswordByCodeCommand request, CancellationToken cancellationToken)
    {
        var code = request.ResetPasswordModel.Code.Base64Decode();

        return await _resetPasswordService.ResetPasswordByCode(request.ResetPasswordModel.Email, code,
            request.ResetPasswordModel.Password);
    }
}

public sealed class ResetPasswordByCodeCommandValidator : AbstractValidator<ResetPasswordByCodeCommand>
{
    public ResetPasswordByCodeCommandValidator()
    {
        RuleFor(x => x.ResetPasswordModel).SetValidator(new ResetPasswordModelValidator());
    }
}