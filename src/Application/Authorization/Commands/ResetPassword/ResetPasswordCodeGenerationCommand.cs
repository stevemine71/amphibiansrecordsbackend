﻿using System.Security.Claims;
using AmphibiansRecordsBackend.Application.Common.Extensions;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using MediatR;

namespace AmphibiansRecordsBackend.Application.Authorization.Commands.ResetPassword;

public record ResetPasswordCodeGenerationCommand(ClaimsPrincipal? ClaimsPrincipal) : IRequest<string>;

public sealed class ResetPasswordCodeGenerationCommandHandler : IRequestHandler<ResetPasswordCodeGenerationCommand, string>
{
    private readonly IEmailSenderService _emailSenderService;
    private readonly IResetPasswordService _resetPasswordService;
    private readonly IServerUrlGeneratorService _serverUrlGeneratorService;

    public ResetPasswordCodeGenerationCommandHandler(IEmailSenderService emailSenderService,
        IServerUrlGeneratorService serverUrlGeneratorService, IResetPasswordService resetPasswordService)
    {
        _emailSenderService = emailSenderService;
        _serverUrlGeneratorService = serverUrlGeneratorService;
        _resetPasswordService = resetPasswordService;
    }

    public async Task<string> Handle(ResetPasswordCodeGenerationCommand request, CancellationToken cancellationToken)
    {
        if (request.ClaimsPrincipal == null)
        {
            return "request.ClaimsPrincipal is null";
        }

        var result =
            await _resetPasswordService.GeneratePasswordResetCode(request.ClaimsPrincipal);
        var userId = result.UserId;

        var code = result.Code.Base64Encode();

        var server = _serverUrlGeneratorService.GetServiceUrl().ToString();
        var url = Path.Combine(server, "api", "EmailConfirmation", userId, code);

        //todo uncomment
        // await _emailSenderService.SendEmailAsync("stevemine71@gmail.com", "Confirm your account",
        //     $"Подтвердите регистрацию, перейдя по ссылке: <a href='{url}'>link</a>");

        return code;
    }
}