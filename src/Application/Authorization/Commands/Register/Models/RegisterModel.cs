﻿using FluentValidation;

namespace AmphibiansRecordsBackend.Application.Authorization.Commands.Register.Models;

public class RegisterModel
{
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string UserName { get; set; }

    public string Email { get; set; }

    public string Password { get; set; }
}

public class RegisterModelValidator : AbstractValidator<RegisterModel>
{
    public RegisterModelValidator()
    {
        RuleFor(x => x.FirstName).NotNull().NotEmpty().MaximumLength(64);
        RuleFor(x => x.LastName).NotNull().NotEmpty().MaximumLength(64);
        RuleFor(x => x.UserName).NotNull().NotEmpty().MaximumLength(64);
        RuleFor(x => x.Email).NotNull().NotEmpty().EmailAddress().MaximumLength(64);
        RuleFor(x => x.Password).NotNull().NotEmpty().MaximumLength(64);
    }
}