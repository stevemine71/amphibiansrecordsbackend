﻿using AmphibiansRecordsBackend.Application.Authorization.Commands.Register.Models;
using AmphibiansRecordsBackend.Application.Authorization.SharedModels;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using FluentValidation;
using MediatR;

namespace AmphibiansRecordsBackend.Application.Authorization.Commands.Register;

public record RegisterCommand(RegisterModel? Register) : IRequest<AuthResponseModel>;

public sealed class RegisterCommandHandler : IRequestHandler<RegisterCommand, AuthResponseModel>
{
    private readonly IIdentityService _identityService;

    public RegisterCommandHandler(IIdentityService identityService)
    {
        _identityService = identityService;
    }
    
    public async Task<AuthResponseModel> Handle(RegisterCommand request, CancellationToken cancellationToken)
    {
        if (request.Register != null)
        {
            return await _identityService.CreateUserAsync(request.Register);
        }

        return new AuthResponseModel { IsAuthenticated = false };
    }
}

public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
{
    public RegisterCommandValidator()
    {
        RuleFor(x => x.Register).SetValidator(new RegisterModelValidator()!);
    }
}