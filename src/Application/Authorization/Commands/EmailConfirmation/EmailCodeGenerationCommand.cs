﻿using System.Security.Claims;
using AmphibiansRecordsBackend.Application.Common.Exceptions;
using AmphibiansRecordsBackend.Application.Common.Extensions;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using MediatR;

namespace AmphibiansRecordsBackend.Application.Authorization.Commands.EmailConfirmation;

public record EmailCodeGenerationCommand(ClaimsPrincipal? ClaimsPrincipal) : IRequest<string>;

public sealed class EmailCodeGenerationCommandHandler : IRequestHandler<EmailCodeGenerationCommand, string>
{
    private readonly IEmailConfirmationService _emailConfirmationService;
    
    private readonly IServerUrlGeneratorService _serverUrlGeneratorService;

    private readonly IConfirmationEmailSenderService _confirmationEmailSenderService;

    public EmailCodeGenerationCommandHandler(IEmailConfirmationService emailConfirmationService, IServerUrlGeneratorService serverUrlGeneratorService, IConfirmationEmailSenderService confirmationEmailSenderService)
    {
        _emailConfirmationService = emailConfirmationService;
        _serverUrlGeneratorService = serverUrlGeneratorService;
        _confirmationEmailSenderService = confirmationEmailSenderService;
    }

    public async Task<string> Handle(EmailCodeGenerationCommand request, CancellationToken cancellationToken)
    {
        if (request.ClaimsPrincipal == null)
        {
            throw new ParameterIsNullException(nameof(request.ClaimsPrincipal));
        }

        var result = await _emailConfirmationService.GenerateEmailConfirmCode(request.ClaimsPrincipal);
        var userId = result.UserId;

        var code = result.Code.Base64Encode();

        var server = _serverUrlGeneratorService.GetServiceUrl().ToString();
        var url = $"{server}api/EmailConfirmation/{userId}/{code}";
        // var url = Path.Combine(server, "api", "EmailConfirmation", userId, code);

        await _confirmationEmailSenderService.SendEmailAsync("stevemine71@gmail.com", "Confirm your account", url);

        return url;
    }
}