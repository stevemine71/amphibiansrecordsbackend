﻿using AmphibiansRecordsBackend.Application.Common.Exceptions;
using AmphibiansRecordsBackend.Application.Common.Extensions;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using FluentValidation;
using MediatR;

namespace AmphibiansRecordsBackend.Application.Authorization.Commands.EmailConfirmation;

public record EmailCodeConfirmationCommand(string UserId, string Code) : IRequest<bool>;

// [Authorize(Roles = "Administrator")]
public sealed class EmailCodeConfirmationCommandHandler : IRequestHandler<EmailCodeConfirmationCommand, bool>
{
    private readonly IEmailConfirmationService _emailConfirmationService;

    public EmailCodeConfirmationCommandHandler(IEmailConfirmationService emailConfirmationService)
    {
        _emailConfirmationService = emailConfirmationService;
    }

    public async Task<bool> Handle(EmailCodeConfirmationCommand request, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(request.Code) || string.IsNullOrEmpty(request.UserId))
        {
            throw new ParameterIsNullException($"{nameof(request.Code)} or {nameof(request.UserId)}");
        }

        var code = request.Code.Base64Decode();

        return await _emailConfirmationService.ConfirmEmailByCode(request.UserId, code);
    }
}

public class EmailCodeConfirmationCommandValidator : AbstractValidator<EmailCodeConfirmationCommand>
{
    public EmailCodeConfirmationCommandValidator()
    {
        RuleFor(x => x.Code).NotNull().NotEmpty();
        RuleFor(x => x.UserId).NotNull().NotEmpty();
    }
}