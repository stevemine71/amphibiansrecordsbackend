﻿using AmphibiansRecordsBackend.Application.Authorization.SharedModels;
using AmphibiansRecordsBackend.Application.Common.Exceptions;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using FluentValidation;
using MediatR;

namespace AmphibiansRecordsBackend.Application.Authorization.Queries.Authorize;

public record AuthorizeQuery(AuthRequestModel? Auth) : IRequest<AuthResponseModel>;

public sealed class AuthorizeQueryHandler : IRequestHandler<AuthorizeQuery, AuthResponseModel>
{
    private readonly IIdentityService _identityService;

    public AuthorizeQueryHandler(IIdentityService identityService)
    {
        _identityService = identityService;
    }

    public async Task<AuthResponseModel> Handle(AuthorizeQuery request, CancellationToken cancellationToken)
    {
        if (request.Auth == null)
        {
            throw new ParameterIsNullException(nameof(request.Auth));
        }
        
        return await _identityService.AuthorizeAsync(request.Auth);
    }
}

public sealed class AuthorizeQueryValidator : AbstractValidator<AuthorizeQuery>
{
    public AuthorizeQueryValidator()
    {
        RuleFor(x => x.Auth).SetValidator(new AuthRequestModelValidator());
    }
}