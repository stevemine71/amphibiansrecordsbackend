﻿namespace AmphibiansRecordsBackend.Application.Common.Exceptions;

public sealed class EmailNotConfirmedException : Exception
{
    public EmailNotConfirmedException(string email) : base($"Email {email} is not confirmed!")
    {
        Email = email;
    }

    public string Email { get; }
}