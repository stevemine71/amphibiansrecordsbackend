﻿namespace AmphibiansRecordsBackend.Application.Common.Exceptions;

public sealed class ForbiddenAccessException : Exception
{
}