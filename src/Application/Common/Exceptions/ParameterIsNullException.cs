﻿namespace AmphibiansRecordsBackend.Application.Common.Exceptions;

public sealed class ParameterIsNullException : Exception
{
    public ParameterIsNullException(string parameterName) : base($"Parameter {parameterName} is null")
    {
        ParameterName = parameterName;
    }

    public string ParameterName { get; }
}