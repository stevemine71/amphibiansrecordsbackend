﻿using System.Security.Claims;
using AmphibiansRecordsBackend.Application.Authorization.Commands.Register.Models;
using AmphibiansRecordsBackend.Application.Authorization.SharedModels;
using AmphibiansRecordsBackend.Application.Common.Models;

namespace AmphibiansRecordsBackend.Application.Common.Interfaces;

public interface IIdentityService
{
    string GetUserIdByClaimsPrincipal(ClaimsPrincipal claimsPrincipal);
    
    Task<string> GetUserNameAsync(string userId);

    Task<bool> IsInRoleAsync(string userId, string role);

    Task<AuthResponseModel> AuthorizeAsync(AuthRequestModel authorizeModel, string policyName = null!);

    Task<AuthResponseModel> CreateUserAsync(RegisterModel registerModel);

    Task<Result> DeleteUserAsync(string userId);

    Task<Result> AddRoleAsync(AddRoleModel model);
}