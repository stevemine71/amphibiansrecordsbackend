﻿using System.Security.Claims;

namespace AmphibiansRecordsBackend.Application.Common.Interfaces;

public interface IEmailConfirmationService
{
    Task<(string UserId, string Code)> GenerateEmailConfirmCode(ClaimsPrincipal claimsPrincipal);

    Task<bool> ConfirmEmailByCode(string userId, string code);
}