﻿namespace AmphibiansRecordsBackend.Application.Common.Interfaces;

public interface IConfirmationEmailSenderService
{
    Task SendEmailAsync(string email, string subject, string code);
}