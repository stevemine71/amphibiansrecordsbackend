﻿namespace AmphibiansRecordsBackend.Application.Common.Interfaces;

public interface IServerUrlGeneratorService
{
    Uri GetServiceUrl();
}