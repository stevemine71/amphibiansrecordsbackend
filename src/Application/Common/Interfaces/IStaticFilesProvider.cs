﻿using AmphibiansRecordsBackend.Domain.Enums;

namespace AmphibiansRecordsBackend.Application.Common.Interfaces;

public interface IStaticFilesProvider
{
    Stream GetFile(StaticFilesEnum staticFilesEnum);
}