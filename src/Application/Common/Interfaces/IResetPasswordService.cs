﻿using System.Security.Claims;

namespace AmphibiansRecordsBackend.Application.Common.Interfaces;

public interface IResetPasswordService
{
    Task<(string UserId, string Code)> GeneratePasswordResetCode(ClaimsPrincipal claimsPrincipal);

    Task<bool> ResetPasswordByCode(string email, string code, string password);
}