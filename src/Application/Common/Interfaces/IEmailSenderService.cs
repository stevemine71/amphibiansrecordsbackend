﻿namespace AmphibiansRecordsBackend.Application.Common.Interfaces;

public interface IEmailSenderService
{
    Task SendEmailAsync(string email, string subject, string message, bool isBodyHtml);
}