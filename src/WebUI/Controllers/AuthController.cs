﻿using AmphibiansRecordsBackend.Application.Authorization.Commands.Register;
using AmphibiansRecordsBackend.Application.Authorization.Commands.Register.Models;
using AmphibiansRecordsBackend.Application.Authorization.Queries.Authorize;
using AmphibiansRecordsBackend.Application.Authorization.SharedModels;
using Microsoft.AspNetCore.Mvc;

namespace AmphibiansRecordsBackend.WebUI.Controllers;

public sealed class AuthController : ApiControllerBase
{
    [HttpPut]
    public async Task<IActionResult> CreateUserAsync([FromBody] RegisterModel model)
    {
        return Ok(await Mediator.Send(new RegisterCommand(model)));
    }

    [HttpPost]
    public async Task<IActionResult> GetTokenAsync([FromBody] AuthRequestModel model)
    {
        return Ok(await Mediator.Send(new AuthorizeQuery(model)));
    }
}