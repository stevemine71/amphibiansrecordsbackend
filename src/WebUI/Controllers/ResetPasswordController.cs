﻿using AmphibiansRecordsBackend.Application.Authorization.Commands.ResetPassword;
using AmphibiansRecordsBackend.Application.Authorization.Commands.ResetPassword.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AmphibiansRecordsBackend.WebUI.Controllers;

[Authorize]
public sealed class ResetPasswordController : ApiControllerBase
{
    [HttpGet]
    public async Task<IActionResult> GeneratePasswordResetCode()
    {
        return Ok(await Mediator.Send(new ResetPasswordCodeGenerationCommand(User)));
    }

    [HttpPost]
    public async Task<IActionResult> ResetPasswordByCode([FromBody] ResetPasswordModel resetPasswordModel)
    {
        return Ok(await Mediator.Send(new ResetPasswordByCodeCommand(resetPasswordModel)));
    }
}