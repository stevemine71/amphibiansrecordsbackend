﻿using AmphibiansRecordsBackend.Application.Authorization.Commands.EmailConfirmation;
using Microsoft.AspNetCore.Mvc;

namespace AmphibiansRecordsBackend.WebUI.Controllers;

public sealed class EmailConfirmationController : ApiControllerBase
{
    [HttpGet]
    public async Task<IActionResult> GenerateEmailConfirmCode()
    {
        return Ok(await Mediator.Send(new EmailCodeGenerationCommand(User)));
    }

    [HttpGet("{userId}/{code}")]
    public async Task<IActionResult> GenerateEmailConfirmCode(string userId, string code)
    {
        return Ok(await Mediator.Send(new EmailCodeConfirmationCommand(userId, code)));
    }
}