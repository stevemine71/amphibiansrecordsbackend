﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace AmphibiansRecordsBackend.WebUI.HealthChecks;

public sealed class HealthCheck : IHealthCheck
{
    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
    {
        return Task.FromResult(HealthCheckResult.Healthy());
    }
}