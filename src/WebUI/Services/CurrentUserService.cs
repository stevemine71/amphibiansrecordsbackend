﻿using AmphibiansRecordsBackend.Application.Common.Interfaces;

namespace AmphibiansRecordsBackend.WebUI.Services;

public sealed class CurrentUserService : ICurrentUserService
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public CurrentUserService(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public string? UserId => _httpContextAccessor.HttpContext?.User?.Claims.FirstOrDefault(a => a.Type == "uid")?.Value;
}