﻿using System.Security.Claims;
using AmphibiansRecordsBackend.Application.Common.Exceptions;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using AmphibiansRecordsBackend.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace AmphibiansRecordsBackend.Infrastructure.Services;

public sealed class EmailConfirmationService : IEmailConfirmationService
{
    private readonly UserManager<ApplicationUser> _userManager;
    
    private readonly IIdentityService _identityService;

    public EmailConfirmationService(UserManager<ApplicationUser> userManager, IIdentityService identityService)
    {
        _userManager = userManager;
        _identityService = identityService;
    }

    public async Task<(string UserId, string Code)> GenerateEmailConfirmCode(ClaimsPrincipal claimsPrincipal)
    {
        var userId = _identityService.GetUserIdByClaimsPrincipal(claimsPrincipal);

        if (string.IsNullOrEmpty(userId))
        {
            throw new NotFoundException("UserId is not found by uid");
        }

        var user = await _userManager.FindByIdAsync(userId);

        if (string.IsNullOrEmpty(userId))
        {
            throw new NotFoundException($"User is not found by userId {userId}");
        }

        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

        return (userId, code);
    }

    public async Task<bool> ConfirmEmailByCode(string userId, string code)
    {
        var user = await _userManager.FindByIdAsync(userId);

        if (user is null)
        {
            throw new NotFoundException(userId);
        }

        var result = await _userManager.ConfirmEmailAsync(user, code);

        return result.Succeeded;
    }
}