﻿using AmphibiansRecordsBackend.Application.Common.Interfaces;

namespace AmphibiansRecordsBackend.Infrastructure.Services;

public sealed class DateTimeService : IDateTime
{
    public DateTime Now => DateTime.Now;
}