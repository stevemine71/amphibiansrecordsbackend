﻿using AmphibiansRecordsBackend.Application.Common.Interfaces;
using AmphibiansRecordsBackend.Domain.Enums;
using HtmlAgilityPack;

namespace AmphibiansRecordsBackend.Infrastructure.Services;

public sealed class ConfirmationEmailSenderService : IConfirmationEmailSenderService
{
    private readonly IEmailSenderService _emailSenderService;
    
    private readonly IStaticFilesProvider _staticFilesProvider;

    public ConfirmationEmailSenderService(IEmailSenderService emailSenderService, IStaticFilesProvider staticFilesProvider)
    {
        _emailSenderService = emailSenderService;
        _staticFilesProvider = staticFilesProvider;
    }

    public async Task SendEmailAsync(string email, string subject, string code)
    {
        var file = _staticFilesProvider.GetFile(StaticFilesEnum.HtmlConfirmMail);
        
        var html = PrepareHtml(file, code);

        await _emailSenderService.SendEmailAsync(email, subject, html, true);
    }
    
    private static string PrepareHtml(Stream stream, string code)
    {
        HtmlDocument doc = new ();
        doc.Load(stream);

        var node = doc.GetElementbyId("code");
        node.SetAttributeValue("href", code);

        return doc.DocumentNode.OuterHtml;
    }
}