﻿using System.Security.Claims;
using AmphibiansRecordsBackend.Application.Common.Exceptions;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using AmphibiansRecordsBackend.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace AmphibiansRecordsBackend.Infrastructure.Services;

public sealed class ResetPasswordService : IResetPasswordService
{
    private readonly UserManager<ApplicationUser> _userManager;

    private readonly IIdentityService _identityService;
    
    public ResetPasswordService(UserManager<ApplicationUser> userManager, IIdentityService identityService)
    {
        _userManager = userManager;
        _identityService = identityService;
    }

    public async Task<(string UserId, string Code)> GeneratePasswordResetCode(ClaimsPrincipal claimsPrincipal)
    {
        var userId = _identityService.GetUserIdByClaimsPrincipal(claimsPrincipal);
        var user = await _userManager.FindByIdAsync(userId);

        if (!await _userManager.IsEmailConfirmedAsync(user))
        {
            throw new EmailNotConfirmedException(user.Email);
        }

        var code = await _userManager.GeneratePasswordResetTokenAsync(user);

        return (userId, code);
    }

    public async Task<bool> ResetPasswordByCode(string email, string code, string password)
    {
        var user = await _userManager.FindByEmailAsync(email);

        if (!await _userManager.IsEmailConfirmedAsync(user))
        {
            throw new EmailNotConfirmedException(email);
        }

        var result = await _userManager.ResetPasswordAsync(user, code, password);

        return result.Succeeded;
    }
}