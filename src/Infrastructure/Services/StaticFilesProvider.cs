﻿using AmphibiansRecordsBackend.Application.Common.Interfaces;
using AmphibiansRecordsBackend.Domain.Enums;

namespace AmphibiansRecordsBackend.Infrastructure.Services;

public sealed class StaticFilesProvider : IStaticFilesProvider
{
    private readonly string _rootPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "wwwroot");
    
    public Stream GetFile(StaticFilesEnum staticFilesEnum)
    {
        var path = Path.Combine(_rootPath, staticFilesEnum.FileName);
        
        //todo обработать filenotfound
        
        return File.OpenRead(path);
    }
}