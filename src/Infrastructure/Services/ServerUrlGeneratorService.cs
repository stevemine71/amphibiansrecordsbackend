﻿using AmphibiansRecordsBackend.Application.Common.Interfaces;
using AmphibiansRecordsBackend.Infrastructure.Common.Options;
using Microsoft.Extensions.Options;

namespace AmphibiansRecordsBackend.Infrastructure.Services;

public sealed class ServerUrlGeneratorService : IServerUrlGeneratorService
{
    private readonly ServerOptions _serverOptions;

    public ServerUrlGeneratorService(IOptions<ServerOptions> serverOptions)
    {
        _serverOptions = serverOptions.Value;
    }

    public Uri GetServiceUrl()
    {
        var scheme = _serverOptions.Scheme;
        var host = _serverOptions.Host;
        var port = _serverOptions.Port;

        return new UriBuilder(scheme, host, port).Uri;
    }
}