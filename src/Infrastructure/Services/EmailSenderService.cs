﻿using System.Net;
using System.Net.Mail;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using AmphibiansRecordsBackend.Infrastructure.Common.Options;
using Microsoft.Extensions.Options;

namespace AmphibiansRecordsBackend.Infrastructure.Services;

public sealed class EmailSenderService : IEmailSenderService
{
    private readonly SmtpOptions _smtpOptions;

    public EmailSenderService(IOptions<SmtpOptions> smtpOptions)
    {
        _smtpOptions = smtpOptions.Value;
    }

    public async Task SendEmailAsync(string email, string subject, string message, bool isBodyHtml)
    {
        using MailMessage mail = new ()
        {
            From = new MailAddress(_smtpOptions.Email),
            Subject = subject,
            IsBodyHtml = isBodyHtml,
            Body = message
        };

        mail.To.Add(new MailAddress(email));

        using SmtpClient client = new()
        {
            Host = _smtpOptions.Host,
            Port = _smtpOptions.Port,
            EnableSsl = true,
            Credentials = new NetworkCredential(_smtpOptions.Email, _smtpOptions.Password)
        };
        
        await client.SendMailAsync(mail);
    }
}