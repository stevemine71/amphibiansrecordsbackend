﻿using AmphibiansRecordsBackend.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AmphibiansRecordsBackend.Infrastructure.Persistence.Configurations;

public sealed class ApplicationUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
{
    public void Configure(EntityTypeBuilder<ApplicationUser> builder)
    {
        builder.Property(t => t.FirstName)
            .HasMaxLength(64)
            .IsRequired();
        
        builder.Property(t => t.LastName)
            .HasMaxLength(64)
            .IsRequired();
    }
}