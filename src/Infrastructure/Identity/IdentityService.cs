﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AmphibiansRecordsBackend.Application.Authorization.Commands.Register.Models;
using AmphibiansRecordsBackend.Application.Authorization.SharedModels;
using AmphibiansRecordsBackend.Application.Common.Interfaces;
using AmphibiansRecordsBackend.Application.Common.Models;
using AmphibiansRecordsBackend.Domain.Entities;
using AmphibiansRecordsBackend.Domain.Enums;
using AmphibiansRecordsBackend.Infrastructure.Common.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace AmphibiansRecordsBackend.Infrastructure.Identity;

public sealed class IdentityService : IIdentityService
{
    private readonly JwtOptions _jwtOptions;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly IUserClaimsPrincipalFactory<ApplicationUser> _userClaimsPrincipalFactory;
    private readonly UserManager<ApplicationUser> _userManager;

    public IdentityService(
        UserManager<ApplicationUser> userManager,
        IUserClaimsPrincipalFactory<ApplicationUser> userClaimsPrincipalFactory,
        IOptions<JwtOptions> jwt, RoleManager<IdentityRole> roleManager)
    {
        _userManager = userManager;
        _userClaimsPrincipalFactory = userClaimsPrincipalFactory;
        _roleManager = roleManager;
        _jwtOptions = jwt.Value;
    }
    
    public string GetUserIdByClaimsPrincipal(ClaimsPrincipal claimsPrincipal)
    {
        return claimsPrincipal?.Claims?.FirstOrDefault(a => a.Type == "uid")?.Value;
    }
    
    public async Task<string> GetUserNameAsync(string userId)
    {
        var user = await _userManager.Users.FirstAsync(u => u.Id == userId);

        return user.UserName;
    }

    public async Task<AuthResponseModel> CreateUserAsync(RegisterModel registerModel)
    {
        if (await _userManager.FindByEmailAsync(registerModel.Email) is not null)
        {
            return new AuthResponseModel { Message = "Email is already registered!" };
        }

        if (await _userManager.FindByNameAsync(registerModel.UserName) is not null)
        {
            return new AuthResponseModel { Message = "Username is already registered!" };
        }

        var user = new ApplicationUser
        {
            UserName = registerModel.UserName,
            Email = registerModel.Email,
            FirstName = registerModel.FirstName,
            LastName = registerModel.LastName
        };

        var result = await _userManager.CreateAsync(user, registerModel.Password);

        if (!result.Succeeded)
        {
            var errors =
                result.Errors.Aggregate(string.Empty, (current, error) => current + $"{error.Description},");

            return new AuthResponseModel { Message = errors };
        }

        await _userManager.AddToRoleAsync(user, UserRoleEnum.User.Name);

        var jwtSecurityToken = await CreateJwtToken(user);

        return new AuthResponseModel
        {
            Email = user.Email,
            ExpiresOn = jwtSecurityToken.ValidTo,
            IsAuthenticated = true,
            Roles = new List<string> { UserRoleEnum.User.Name },
            Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
            UserName = user.UserName
        };
    }

    public async Task<Result> AddRoleAsync(AddRoleModel model)
    {
        var user = await _userManager.FindByIdAsync(model.UserId);

        if (user is null || !await _roleManager.RoleExistsAsync(model.Role))
        {
            return Result.Failure(new []{ "Invalid user ID or Role" });
        }

        if (await _userManager.IsInRoleAsync(user, model.Role))
        {
            return Result.Failure(new []{ "User already assigned to this role" });
        }

        var result = await _userManager.AddToRoleAsync(user, model.Role);

        return result != null ? result.ToApplicationResult() : Result.Failure(new []{ "Result is null" });
    }

    public async Task<bool> IsInRoleAsync(string userId, string role)
    {
        var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

        return user != null && await _userManager.IsInRoleAsync(user, role);
    }

    public async Task<AuthResponseModel> AuthorizeAsync(AuthRequestModel authorizeModel, string policyName = null!)
    {
        var user = await _userManager.FindByEmailAsync(authorizeModel.Email);

        if (user == null)
        {
            return new AuthResponseModel { IsAuthenticated = false };
        }

        var result = new AuthResponseModel();

        if (!await _userManager.CheckPasswordAsync(user, authorizeModel.Password))
        {
            result.Message = "Email or Password is incorrect!";
            return result;
        }

        var jwtSecurityToken = await CreateJwtToken(user);
        var rolesList = await _userManager.GetRolesAsync(user);

        result.IsAuthenticated = true;
        result.Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        result.Email = user.Email;
        result.UserName = user.UserName;
        result.ExpiresOn = jwtSecurityToken.ValidTo;
        result.Roles = rolesList.ToList();

        //todo
        //var principal = await _userClaimsPrincipalFactory.CreateAsync(user);
        //var result = await _authorizationService.AuthorizeAsync(principal, policyName);

        return result;
    }

    public async Task<Result> DeleteUserAsync(string userId)
    {
        var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

        return user != null ? await DeleteUserAsync(user) : Result.Success();
    }

    private async Task<Result> DeleteUserAsync(ApplicationUser user)
    {
        var result = await _userManager.DeleteAsync(user);

        return result.ToApplicationResult();
    }

    private async Task<JwtSecurityToken> CreateJwtToken(ApplicationUser user)
    {
        var userClaims = await _userManager.GetClaimsAsync(user);
        var roles = await _userManager.GetRolesAsync(user);
        var roleClaims = roles.Select(role => new Claim("roles", role)).ToList();

        var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email), new Claim("uid", user.Id)
            }
            .Union(userClaims)
            .Union(roleClaims);

        var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Key));
        var signingCredentials =
            new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

        var jwtSecurityToken = new JwtSecurityToken(
            _jwtOptions.Issuer,
            _jwtOptions.Audience,
            claims,
            expires: DateTime.Now.AddDays(_jwtOptions.DurationInDays),
            signingCredentials: signingCredentials);

        return jwtSecurityToken;
    }
}