﻿namespace AmphibiansRecordsBackend.Infrastructure.Common.Options;

public sealed class ServerOptions
{
    public string Host { get; set; }

    public int Port { get; set; }

    public string Scheme { get; set; }
}